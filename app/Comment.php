<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    protected $fillable=[
        'body'
    ];

    public function User(){
        return $this->belongsTo(User::class);
//        return $this->belongsTo('\App\User','id','user_id');
    }
}
